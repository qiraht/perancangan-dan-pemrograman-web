from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('susunjadwal/', views.jadwal, name='jadwal'),
    path('navbar', views.navbar, name='navbar'),
    path('daftar_jadwal/', views.daftar_jadwal, name='daftar_jadwal'),
    # path('<id>/hapus/', views.hapus, name="hapus"),
    path('kegiatan/', views.kegiatann, name='kegiatan'),
    path('daftar_kegiatan/', views.daftar_kegiatan, name='daftar_kegiatan'),
    path('akordion/', views.akordion, name='akordion'),
]
