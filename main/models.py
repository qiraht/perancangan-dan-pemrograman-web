from django.db import models

# Create your models here.
class data_matkul(models.Model):
    matkul = models.CharField(max_length=50)
    dosen_pengajar = models.CharField(max_length=70)
    jumlah_sks = models.PositiveIntegerField(null=True)
    deskripsi_matkul = models.CharField(max_length=150)
    smt_tahun = models.CharField(max_length=50)
    ruang_kelas = models.CharField(max_length=20)

class kegiatan(models.Model):
    anggota = models.CharField(max_length=100)