from django.contrib import admin
from .models import kegiatan, data_matkul

# Register your models here.
admin.site.register(kegiatan)
admin.site.register(data_matkul)
