from django import forms
from .models import data_matkul, kegiatan

class Input_forms(forms.ModelForm):
    class Meta:
        model = data_matkul
        fields = ['matkul',
        'dosen_pengajar',
        'jumlah_sks',
        'deskripsi_matkul',
        'smt_tahun',
        'ruang_kelas']

    error_messages = {
        'required' : 'Harap diisi'
    }

    matkul = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput(attrs= {'placeholder': 'Nama mata kuliah'}))
    dosen_pengajar = forms.CharField(label='', required=True, max_length=70, widget=forms.TextInput(attrs= {'placeholder': 'Nama Dosen'}))
    jumlah_sks = forms.IntegerField(label='', required=True, min_value=0, widget=forms.NumberInput(attrs= {'class': 'form-control'}))
    deskripsi_matkul = forms.CharField(label='', required=True, max_length=150, widget=forms.TextInput(attrs= {'placeholder': 'Deskripsi mata kuliah'}))
    smt_tahun = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput(attrs= {'placeholder': 'Tahun ajaran'}))
    ruang_kelas = forms.CharField(label='', required=True, max_length=20, widget=forms.TextInput(attrs= {'placeholder': 'Ruang kelas'}))

class Input_anggota(forms.ModelForm):
    class Meta:
        model = kegiatan
        fields = ['anggota',]
            
    error_messages = {
        'required' : 'Harap diisi'
    }

    anggota = forms.CharField(label='', required=True, max_length=100, widget=forms.TextInput(attrs= {'placeholder': 'Masukkan nama'}))