from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from .models import data_matkul, kegiatan
from .forms import Input_forms, Input_anggota

def home(request):
    return render(request, 'main/home.html')

def about(request):
    return render(request, 'main/about.html')

def navbar(request):
    return render(request,'main/navbar.html')

def coba(request):
    return render(request, 'main/coba.html')

# Susun Jadwal
response = {}
def jadwal(request):
    form = Input_forms(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        form.save()
        return redirect('/daftar_jadwal')
    response['daftar_jadwal'] = form
    return render(request, 'main/jadwal.html', response)

def daftar_jadwal(request):
    jadwal = data_matkul.objects.all()
    response['jadwal'] = jadwal
    return render(request, 'main/daftar_jadwal.html',response)

# Kegiatan/Aktivitas
def daftar_kegiatan(request):
    form = Input_anggota(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        form.save()
        return redirect('/kegiatan')
    response['daftar_kegiatan'] = form
    return render(request, 'main/daftar_kegiatan.html', response)

def kegiatann(request):
    aktivitas = kegiatan.objects.all()
    response['aktivitas'] = aktivitas
    return render(request, 'main/kegiatan.html',response)

def akordion(request):
    return render(request, 'main/accordion.html')
    
# def hapus(request, id):
#     obj = get_object_or_404(data_matkul, id=id)
#     if request.method == 'POST':
#         obj.delete()
#         return redirect('/daftar_jadwal')
#     return render(request, 'main/daftar_jadwal.html', response)