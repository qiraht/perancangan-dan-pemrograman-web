from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
# from .models import data_mhs
from django.http import response
# from .views import *
# from .urls import *

class Test_URL(TestCase):
    def test_apakah_url_about_ada(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code,200)
    def test_apakah_url_home_ada(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

# class TestModels(TestCase):
#     def test_model(self):  
#         data_mhs.objects.create(nama_mhs='Farhan', npm_mhs=1906399215)
#         data_mhs.objects.create(nama_mhs='fulan',npm_mhs=1908765432)
#         hitung_jumlah = data_mhs.objects.all().count()
#         self.assertEqual(hitung_jumlah,2)

class TestAssertDjango(TestCase):
    def test_about_using_about_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'main/about.html')
    def test_about_using_home_template(self):
        response = Client().get("/")
        self.assertTemplateUsed(response,'main/home.html')
        self.assertContains(response, 'Nama')
        self.assertNotContains(response, 'catur')
